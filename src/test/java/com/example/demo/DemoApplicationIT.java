package com.example.demo;

import com.example.demo.controller.ToDoController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DemoApplicationIT {

    @Autowired
    private ToDoController toDoController;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() throws Exception {
        if (toDoController == null) {
            throw new Exception("ToDoController is null");
        }
    }

    @Test
    public void myContextLoads() {
        assertThat(toDoController).isNotNull();
    }

    @Test
    public void returnText() throws Exception {
        this.mockMvc.perform(get("/test")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("test test")));
    }
}
