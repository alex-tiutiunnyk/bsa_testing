package com.example.demo.controller;

import com.example.demo.dto.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.service.ToDoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoService toDoService;

    @Test
    void whenGetAll_thenReturnValidResponse() throws Exception {
        String testText = "My to do text";
        Long testId = 1l;
        when(toDoService.getAll()).thenReturn(
                Arrays.asList(
                        ToDoEntityToResponseMapper.map(new ToDoEntity(testId, testText))
                )
        );

        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].text").value(testText))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(testId))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    public void whenGetOne_thenThrowNotFoundException() throws Exception {
        long id = 3L;
        when(toDoService.getOne(id)).thenThrow(new ToDoNotFoundException(id));

        mockMvc.perform(get("/todos/{id}", id))
                .andExpect(status().isOk());

        verify(toDoService, times(1)).getOne(id);
        verifyNoMoreInteractions(toDoService);
    }

    @Test
    public void whenTest_thenReturnMessage() throws Exception {
        String mockTest = "test Mock";
        when(toDoService.test()).thenReturn(mockTest);

        this.mockMvc.perform(get("/test")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(mockTest)));
    }

    @Test
    public void whenGetOneCompleted_thenReturnEntity() throws Exception {
        var todo = new ToDoEntity(1L, "Test 1");
        todo.completeNow();
        when(toDoService.getOneCompleted(1L)).thenReturn(ToDoEntityToResponseMapper.map(todo));

        this.mockMvc
                .perform(get("/todos/completed/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isNotEmpty())
                .andExpect(jsonPath("$.text").value("Test 1"))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.completedAt").exists());

        verify(toDoService, times(1)).getOneCompleted(1L);
        verifyNoMoreInteractions(toDoService);
    }
}
